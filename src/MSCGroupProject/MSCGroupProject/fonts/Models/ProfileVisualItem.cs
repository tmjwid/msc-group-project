﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.Data.Interfaces;

namespace MSCGroupProject.Models
{
    public class ProfileVisualItem
    {
        public Profile Profile { get; set; }
        public List<VisualItem> VisualItems {get; set;}
    }
}