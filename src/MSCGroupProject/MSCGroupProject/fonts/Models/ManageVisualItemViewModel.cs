﻿using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSCGroupProject.Models
{
    public class ManageVisualItemViewModel
    {
        public VisualItem VisualItem { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<GeoFence> GeoFence { get; set; }
    }

    public class ManageProfileViewModel
    {
        public Profile Profile { get; set; }
        public List<VisualItem> VisualItems { get; set; }
        public List<GeoFence> GeoFence { get; set; }
    }
    public class ManageUserViewModel
    {
        public User User { get; set; }
        public List<Profile> Profiles { get; set; }
    }
}