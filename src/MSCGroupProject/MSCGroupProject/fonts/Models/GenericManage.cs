﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSCGroupProject.Models
{
    public class GenericManage<I,L>
    {
        public I Item { get; set; }
        public L List { get; set; }
    }
}