﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSCGroupProject.Filter
{
    public class ATAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// this function used to check if the user is authenticated
        /// when the user is not authenticated then users is send to the login page
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("~/User/LogIn");
                return;
            }

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectResult("~/User/LogIn");
                return;
            }
        }
    }
}