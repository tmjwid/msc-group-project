﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Models;
using System.Web.Security;

namespace MSCGroupProject.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepositiory _userRepository;
        private readonly IProfileRepostiory _profileRepository;

        public UserController(IUserRepositiory userRepository, IProfileRepostiory profileRepository)
        {
            _userRepository = userRepository;
            _profileRepository = profileRepository;
        }
        [Authorize]
        // GET: User
        public ActionResult Index()
        {
            var model = _userRepository.GetAllUsers();
            return View(model);
        }
        [Authorize]
        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            var model = _userRepository.FindUserByID(id);
            return View(model);
        }
        /// <summary>
        /// manage used to provide interface with manage assosiated profiles for the user
        /// need to register to be able access
        /// </summary>
        /// <returns>return manage page</returns>
        [Authorize]
        public ActionResult Manage()
        {
            if (HttpContext.Session["UserId"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            int userId = (int)HttpContext.Session["UserId"];
            ManageUserViewModel userViewModel = new ManageUserViewModel
            {
                User = _userRepository.FindUserByID(userId),
                Profiles = _profileRepository.GetProfilesNotInUser(userId)
            };

            return View(userViewModel);
        }
        /// <summary>
        /// this function will add new profile to the database
        /// and associate the new profile to the currently logged in the user
        /// </summary>
        /// <param name="collection"> contained relavent infomration about the user
        /// and the profile</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult AddNewProfile(FormCollection collection)
        {
            int userId = int.Parse(collection["userId"]);

            bool isDefault; 
            bool.TryParse(collection["IsDefault"], out isDefault);

            Profile newProfile = new Profile();
            newProfile.Name = collection["Name"];
            newProfile.Catagory = collection["Catagory"];
            newProfile.IsDefault = isDefault;

            int id = _profileRepository.Insert(newProfile);

            if (id != 0)
            {
                _userRepository.MangeUserProfiles(userId, id, true);
            }
            return RedirectToAction("Manage", new { Id = userId });
        }
        /// <summary>
        /// this section uused to added or remove existing profiles to the current user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="profileId"></param>
        /// <param name="add"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult ManageProfiles(int userId, int profileId, bool add)
        {
            _userRepository.MangeUserProfiles(userId, profileId, add);
            return RedirectToAction("Manage", new { Id = userId });
        }
       
        // GET: User/Create
        /// <summary>
        /// display the view, where user can enter their details to register
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        // POST: User/Create


        /// <summary>
        /// this action obtains and validate the details and inserts a new user
        /// to the database
        /// </summary>
        /// <param name="user">user details</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(User user)
        {
            try
            {
                // TODO: Add insert logic here
                user.SetPassword(user.Password);
                _userRepository.Insert(user);
                return RedirectToAction("Index","Home");
            }
            catch
            {
                return View();
            }
        }
        /// <summary>
        /// provide the view to obtain login details
        /// </summary>
        /// <returns></returns>
        public ActionResult Login() 
        {
            return View();
        }
        /// <summary>
        /// this function obtains the login detail for the user 
        /// and checks the details to allow acess the application
        /// </summary>
        /// <param name="login"> user details</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            var loggedInUser = _userRepository.Login(login.UserName, login.Password, false);
            if (loggedInUser != null)
            {
                HttpContext.Session.Add("User", loggedInUser);
                HttpContext.Session.Add("UserId", loggedInUser.Id);

                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, loggedInUser.Username, DateTime.Now, DateTime.Now.AddMinutes(20), false,
                    "User", FormsAuthentication.FormsCookiePath);
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                Response.Cookies.Add(cookie);
                return RedirectToAction("Index", "Home");
            }

            return View();
        }
        /// <summary>
        /// this action is to allow users to sign out of the current sessoin
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            HttpContext.Session.Remove("User");
            return RedirectToAction("Login", "User");
        }
    }
}
