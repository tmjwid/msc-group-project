﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Filter;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSCGroupProject.Controllers
{
    [ATAuthorize]
    public class ProfileController : Controller
    {
        //repositories will be used by this controller
        private readonly IProfileRepostiory _profileRepository;
        private readonly IVisualItemRepository _visualItemRepository;
        private readonly IGeoFenceRepositiory _geofenceRepository;

        /// <summary>
        /// initialising the controllers 
        /// </summary>
        /// <param name="profileRepository"></param>
        /// <param name="visualItemRepository"></param>
        /// <param name="geofenceRepository"></param>
        public ProfileController(IProfileRepostiory profileRepository,IVisualItemRepository visualItemRepository,
            IGeoFenceRepositiory geofenceRepository)
        {
            _profileRepository = profileRepository;
            _visualItemRepository = visualItemRepository;
            _geofenceRepository = geofenceRepository;
        }
        // GET: Profile
        /// <summary>
        /// get all the profiles in the database
        /// </summary>
        /// <returns>index view with all the visualItems</returns>
        public ActionResult Index()
        {
            var model = _profileRepository.GetAllProfiles();
            return View(model);
        }

        // GET: Profile/Create
        public ActionResult Create()
        {
            return View(new Profile());
        }

        // POST: Profile/Create
        /// <summary>
        /// this functions get all the details for profile
        /// and inserts a new profile object to the database
        /// after validatation
        /// </summary>
        /// <param name="visualItem"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Profile profile)
        {
            try
            {
                // TODO: Add insert logic here
                _profileRepository.Insert(profile);
                return RedirectToAction("Index", "Profile");
            }
            catch
            {
                return View();
            }
        }
        /// <summary>
        /// manage used to provide interface with manage assosiated visualItems and geofence for the given profile
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult Manage(int id)
        {
            ManageProfileViewModel profile = new ManageProfileViewModel()
            {
                Profile = _profileRepository.FindProfileByID(id),
                VisualItems = _visualItemRepository.GetVisualItemsNotInProfile(id).ToList(),
                GeoFence = _geofenceRepository.GetGeoFencesNotInProfile(id).ToList(),
            };
            return View(profile);
        }
        /// <summary>
        /// ManageGeofence used to add or remove visualItem for the given profile
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult ManageVisualItem(int profileId, int visualItemId, bool add)
        {
            _profileRepository.MangeProfileVisualItem(profileId, visualItemId, add);
            return RedirectToAction("Manage", new { id = profileId });
        }
        /// <summary>
        /// ManageGeofence used to add or remove geofence for the given profile
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult ManageGeofence(int profileId, int geofenceId, bool add)
        {
            _profileRepository.MangeProfileGeofence(profileId, geofenceId, add);
            return RedirectToAction("Manage", new { id = profileId });
        }

        /// <summary>
        /// this function will add new visualItem to the database
        /// and associate the new visualItem to the given profile
        /// </summary>
        /// <param name="collection"> contained relavent infomration about the visualItem
        /// and the profile</param>
        /// <param name="image"></param>
        /// <returns></returns>
        public ActionResult AddNewVisualItem(FormCollection collection, HttpPostedFileBase image)
        {
            bool isPrivate;
            bool.TryParse(collection["IsPrivate"], out isPrivate);

            int profileId = int.Parse(collection["profileId"]);

            VisualItem newVisualItem = new VisualItem();
            newVisualItem.Name = collection["Name"];
            newVisualItem.Description = collection["Description"];
            newVisualItem.Text = collection["Text"];
            newVisualItem.IsPrivate = isPrivate;

            using (var reader = new System.IO.BinaryReader(image.InputStream))
            {
                newVisualItem.Upload = reader.ReadBytes(image.ContentLength);
            }


            int id = _visualItemRepository.Insert(newVisualItem);
            if (id != 0)
            {
                _profileRepository.MangeProfileVisualItem(profileId, id, true);
            }
            return RedirectToAction("Manage", new { id = profileId });
        }

    }
}
