﻿using MSCGroupProject.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSCGroupProject.Controllers
{
    public class HomeController : Controller
    {

        private readonly IUserRepositiory _userRepositiory;

        public HomeController(IUserRepositiory userRepositiory)
        {
            _userRepositiory = userRepositiory;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}