﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Filter;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSCGroupProject.Controllers
{
    [ATAuthorize]
    public class VisualItemController : Controller
    {
        //repositories will be used by this controller
        private readonly IVisualItemRepository _visualItemRepository;
        private readonly IGeoFenceRepositiory _geofenceRepository;
        private readonly IProfileRepostiory _profileRepository;
        /// <summary>
        /// initialising the controllers 
        /// </summary>
        /// <param name="visualItemRepository"></param>
        /// <param name="profileRepository"></param>
        /// <param name="geofenceRepository"></param>
        public VisualItemController(IVisualItemRepository visualItemRepository, IProfileRepostiory profileRepository,
            IGeoFenceRepositiory geofenceRepository)
        {
            _visualItemRepository = visualItemRepository;
            _profileRepository = profileRepository;
            _geofenceRepository = geofenceRepository;
        }
        // GET: VisualItem
        /// <summary>
        /// get all the visualItems in the database
        /// </summary>
        /// <returns>index view with all the visualItems</returns>
        public ActionResult Index()
        {
            var model = _visualItemRepository.GetAllVisualItems();
            return View(model);
        }
        // GET: VisualItem/Create
        public ActionResult Create()
        {
            return View(new VisualItem());
        }

        // POST: VisualItem/Create
        /// <summary>
        /// get all the details for visualITems, which is validated
        /// and inserts a new visualitem object to the database
        /// </summary>
        /// <param name="visualItem"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(VisualItem visualItem, HttpPostedFileBase image)
        {
            try
            {
                using (var reader = new System.IO.BinaryReader(image.InputStream))
                {
                    visualItem.Upload = reader.ReadBytes(image.ContentLength);
                }
                _visualItemRepository.Insert(visualItem);
                return RedirectToAction("Index","VisualItem");
            }
            catch
            {
                return View();
            }
        }
        /// <summary>
        /// manage used to provide interface with manage assosiated profiels and geofence for the given visualItem
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult Manage(int Id)
        {
            ManageVisualItemViewModel visualItemProfile = new ManageVisualItemViewModel()
            {
                VisualItem = _visualItemRepository.FindVisualItemByID(Id),
                Profiles = _profileRepository.GetProfilesNotInVisualItem(Id).ToList(),
                GeoFence = _geofenceRepository.GetGeoFencesNotInVisualItem(Id).ToList(),
            };
            return View(visualItemProfile);
        }
        /// <summary>
        /// ManageGeofence used to add or remove profile for the given visualItem
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult ManageProfiles(int visualItemId, int profileId, bool add)
        {
            _visualItemRepository.MangeProfileVisualItem(visualItemId, profileId, add);
            return RedirectToAction("Manage", new { Id = visualItemId });
        }
        /// <summary>
        /// ManageGeofence used to add or remove geofence for the given visualItem
        /// </summary>
        /// <returns>return manage page</returns>
        public ActionResult ManageGeofence(int visualItemId, int geofenceId, bool add)
        {
            _visualItemRepository.MangeGeoFenceVisualItem(visualItemId, geofenceId, add);
            return RedirectToAction("Manage", new { Id = visualItemId });
        }
        /// <summary>
        /// this function will add new profile to the database
        /// and associate the new profile to the currently logged in the user
        /// </summary>
        /// <param name="collection"> contained relavent infomration about the visualItem
        /// and the profile</param>
        /// <returns></returns>
        public ActionResult AddNewProfile(FormCollection collection)
        {
            int visualItemId = int.Parse(collection["visualItemId"]);

            bool isDefault;
            bool.TryParse(collection["IsDefault"], out isDefault);

            Profile newProfile = new Profile();
            newProfile.Name = collection["Name"];
            newProfile.Catagory = collection["Catagory"];
            newProfile.IsDefault = isDefault;

            
            int id = _profileRepository.Insert(newProfile);
            if (id != 0)
            {
                _visualItemRepository.MangeProfileVisualItem(visualItemId, id, true);
            }
            return RedirectToAction("Manage", new { Id = visualItemId });
        }
    }
}
