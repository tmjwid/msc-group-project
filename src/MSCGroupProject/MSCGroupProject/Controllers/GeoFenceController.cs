﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Filter;
using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSCGroupProject.Controllers
{
    [ATAuthorize]
    public class GeoFenceController : Controller
    {
        //repositories will be used by this controller
        private readonly IGeoFenceRepositiory _geoFenceRepository;
        private readonly IProfileRepostiory _profileRepository;

        /// <summary>
        /// initialising the controllers 
        /// </summary>
        /// <param name="geoFenceRepository"></param>
        /// <param name="profileRepository"></param>
        public GeoFenceController(IGeoFenceRepositiory geoFenceRepository, IProfileRepostiory profileRepository)
        {
            _geoFenceRepository = geoFenceRepository;
            _profileRepository = profileRepository;

        }

        // GET: GeoFence
        /// <summary>
        /// get all the geofence in the database
        /// </summary>
        /// <returns>index view with all the geofence</returns>
        public ActionResult Index()
        {
            var model = _geoFenceRepository.GetAllGeoFences();
            return View(model);
        }

        // GET: GeoFence/Create
        public ActionResult Create()
        {
            return View(new GeoFence());
        }

        // POST: GeoFence/Create
        [HttpPost]
        public ActionResult Create(GeoFence geoFence)
        {
            try
            {
                // TODO: Add insert logic here
                int insert = _geoFenceRepository.Insert(geoFence);
                if (insert !=0)
                {
                    return View(new GeoFence());
                }
                return RedirectToAction("Index","GeoFence");
            }
            catch
            {
                return View();
            }
        }
    }
}
