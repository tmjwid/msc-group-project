﻿using DevOne.Security.Cryptography.BCrypt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Framework.Domain
{
    public class User
    {
        private string _password;

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
            }
        }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string UserToken { get; set; }
        public List<Profile> Profiles { get; set; }
        public bool IsCorrectPassword(string password)
        {
            bool isPassword = BCryptHelper.CheckPassword(password, _password);
            _password = null;
            return isPassword;
        }

        public void SetPassword(string password)
        {
            _password = BCryptHelper.HashPassword(password, BCryptHelper.GenerateSalt());
        }
    }
}
