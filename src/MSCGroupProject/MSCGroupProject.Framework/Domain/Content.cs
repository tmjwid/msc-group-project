﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Framework.Domain
{
    public class Content
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public List<VisualItem> VisualItems { get; set; }
    }
}
