﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Framework.Domain
{
    public class Profile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Catagory { get; set; }
        public bool IsDefault { get; set; }
        public List<VisualItem> VisualItems { get; set; }
        public List<GeoFence> GeoFences { get; set; }
    }
}
