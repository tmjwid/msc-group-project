﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Framework.Domain
{
    public class GeoFence
    {
        public int Id { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Radius { get; set; }
        public string Name { get; set; }

        public List<VisualItem> VisualItems { get; set; }
        public List<Profile> Profiles { get; set; }
    }
}
