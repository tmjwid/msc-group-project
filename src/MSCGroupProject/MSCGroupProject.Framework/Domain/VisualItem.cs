﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Framework.Domain
{
    public class VisualItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public byte[] Upload { get; set; }
        public bool IsPrivate { get; set; }
        public List<Profile> Profiles { get; set; }
        public List<GeoFence> GeoFences { get; set; }

    }
}
