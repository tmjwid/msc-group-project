﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MSCGroupProject.Data.Interfaces;

namespace MSCGroupProject.Data.Repositories
{

    /// <summary>
    /// http://www.tugberkugurlu.com/archive/generic-repository-pattern-entity-framework-asp-net-mvc-and-unit-testing-triangle
    /// </summary>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="T"></typeparam>
    public abstract class GenericRepository<C, T> 
        where T : class
        where C : DbContext, new()
    {
        protected GenericRepository()
        {
            Mappings.MappingHelper.CreateMappings();
        }

        private C _entities = new C();
        protected C Context
        {
            get { return _entities; }
            set { _entities = value; }
        }

        protected DbSet<T> Table
        {
            get { return _entities.Set<T>(); }
        }

        protected virtual List<T> GetAll()
        {
            List<T> query = _entities.Set<T>().ToList();
            return query;
        }

        protected List<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            List<T> query = _entities.Set<T>().Where(predicate).ToList();
            return query;
        }

        protected T FindSingle(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            T query = _entities.Set<T>().FirstOrDefault(predicate);
            return query;
        }

        protected virtual bool Add(T entity)
        {
            try
            {
                _entities.Set<T>().Add(entity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected virtual bool Delete(T entity)
        {
            try
            {
                _entities.Set<T>().Remove(entity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected virtual bool Edit(T entity)
        {
            try
            {
                _entities.Entry(entity).State = EntityState.Modified;
                return true;
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException)
            {
                return false;
            }
        }

        protected virtual int Save()
        {
            try
            {
                return _entities.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        //raise a new exception inserting the current one as the InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
    }
}