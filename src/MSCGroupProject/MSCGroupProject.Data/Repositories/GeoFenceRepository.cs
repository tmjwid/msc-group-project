﻿using AutoMapper;
using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Repositories
{
    public class GeoFenceRepository : GenericRepository<GroupProjectModelContainer, GeoFence>, IGeoFenceRepositiory
    {
        /// <summary>
        /// Insert and save a geofence to the database
        /// this function will map the geofence to the database entity 
        /// and saves it to the database
        /// </summary>
        /// <param name="geoFence"> geofence to be saved</param>
        /// <returns>return the newly added geofence ID</returns>
        public int Insert(Framework.Domain.GeoFence geoFence)
        {
            try
            {
                GeoFence geoFenceToInsert = Mapper.Map<GeoFence>(geoFence);
                Add(geoFenceToInsert);
                Save();
                return geoFenceToInsert.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// update a geofence object in the database
        /// </summary>
        /// <param name="geofence">geofence with the updates</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Edit(Framework.Domain.GeoFence geofence)
        {
            try
            {
                GeoFence geofenceToEdit = Mapper.Map<GeoFence>(geofence);
                Edit(geofenceToEdit);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// delete a geofence object from the database
        /// </summary>
        /// <param name="id">id to obtain the geofence</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Delete(int id)
        {
            var list = Table.Where(v => v.VisualItems.Any(p => p.Id == ID));
            List<MSCGroupProject.Data.Model.GeoFence> geofence = GetAll();
            foreach (var ls in list)
            {
                geofence.Remove(ls);
            }
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.GeoFence>>(geofence);
        }
        public List<Framework.Domain.GeoFence> GetGeoFencesNotInProfile(int id)
        {
            List<MSCGroupProject.Data.Model.GeoFence> geofence = Table.Where(g => !g.Profiles.Any(p => p.Id == id)).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.GeoFence>>(geofence);
        }
        public Framework.Domain.GeoFence FindGeoFenceByID(int ID)
        {
            return LoadSingleGeoFence(FindSingle(g => g.Id == ID));
        }

        internal Framework.Domain.GeoFence LoadSingleGeoFence(GeoFence geoFenceToMap)
        {
            return Mapper.Map<Framework.Domain.GeoFence>(geoFenceToMap);
        }
        internal List<MSCGroupProject.Framework.Domain.GeoFence> LoadGeofences(List<GeoFence> geoFenceToMap)
        {
            return Mapper.Map<List<GeoFence>, List<MSCGroupProject.Framework.Domain.GeoFence>>(geoFenceToMap);
        }
        public bool Edit(Framework.Domain.GeoFence geofence)
        {
            try
            {
                var geofence = Table.FirstOrDefault(g => g.Id == id);
                Delete(geofence);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// get a specific geofence based on the ID
        /// </summary>
        /// <param name="ID">id to obtain the correct geofence</param>
        /// <returns>return geofence</returns>
        public Framework.Domain.GeoFence FindGeoFenceByID(int ID)
        {
            return LoadSingleGeoFence(FindSingle(g => g.Id == ID));
        }

        public List<Framework.Domain.GeoFence> GetAllGeoFences()
        {
            try
            {
                return LoadGeofences(GetAll());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// this function used to object any geofence associated with the specific visualItem
        /// </summary>
        /// <param name="visualItemId">to indicate which visualItem</param>
        /// <returns>collection of geofence</returns>
        public List<Framework.Domain.GeoFence> GetGeoFencesForVisualItem(int visualItemId)
        {
            List<MSCGroupProject.Data.Model.GeoFence> lstGeofence = GetAll()
                 .Where(x => x.Profiles.Any(p => p.Id == visualItemId))
                .ToList();
            return LoadGeofences(lstGeofence);
        }
        /// <summary>
        /// this function used to object any geofence associated with the specific profile
        /// </summary>
        /// <param name="profileId">to indicate which profileId</param>
        /// <returns>collection of geofence</returns>
        public List<Framework.Domain.GeoFence> GetGeoFencesForProfile(int profileId)
        {
            List<MSCGroupProject.Data.Model.GeoFence> lstGeofence = GetAll()
                 .Where(x => x.Profiles.Any(p => p.Id == profileId))
                 .ToList();
            return LoadGeofences(lstGeofence);
        }
        /// <summary>
        /// this function used to get any geofence not assosiated with given visualItem
        /// to identify which geofence can be added to the visualItem
        /// </summary>
        /// <param name="visualItemId">its the visualItems ID </param>
        /// <returns>collections of geofence that is currently not assied to the give visualItem</returns>
        public List<Framework.Domain.GeoFence> GetGeoFencesNotInVisualItem(int visualItemId)
        {
            List<MSCGroupProject.Data.Model.GeoFence> lstGeofence = GetAll().Except(Context.VisualItems.FirstOrDefault(v => v.Id == visualItemId).GeoFences).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.GeoFence>>(lstGeofence);
        }
        /// <summary>
        /// this function used to get any geofence not assosiated with given profile
        /// to identify which geofence can be added to the profile
        /// </summary>
        /// <param name="profileId">its the profile ID </param>
        /// <returns>collections of geofence that is currently not assied to the give profile</returns>
        public List<Framework.Domain.GeoFence> GetGeoFencesNotInProfile(int profileId)
        {
            List<MSCGroupProject.Data.Model.GeoFence> lstGeofence = GetAll().Except(Context.Profiles.FirstOrDefault(v => v.Id == profileId).GeoFences).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.GeoFence>>(lstGeofence);
        }
        /// <summary>
        /// Map a Framework object to Database object using the Automapper
        /// </summary>
        /// <param name="geoFenceToMap"> the geofence object to map</param>
        /// <returns>framework geofence object</returns>
        internal Framework.Domain.GeoFence LoadSingleGeoFence(GeoFence geoFenceToMap)
        {
            return Mapper.Map<Framework.Domain.GeoFence>(geoFenceToMap);
        }
        /// <summary>
        /// Map collection of Framework objects to Database objects using the Automapper
        /// </summary>
        /// <param name="geoFenceToMap"> the geofence objects to map</param>
        /// <returns>collection of framework geofence objects</returns>
        internal List<MSCGroupProject.Framework.Domain.GeoFence> LoadGeofences(List<GeoFence> geoFenceToMap)
        {
            return Mapper.Map<List<GeoFence>, List<MSCGroupProject.Framework.Domain.GeoFence>>(geoFenceToMap);
        }
    }
}
