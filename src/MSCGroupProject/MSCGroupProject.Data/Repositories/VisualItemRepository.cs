﻿using AutoMapper;
using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Repositories
{
    public class VisualItemRepository : GenericRepository<GroupProjectModelContainer, VisualItem>, IVisualItemRepository
    {
        /// <summary>
        /// Insert and save a visualItem to the database
        /// this function will map the profile to the database entity 
        /// and saves it to the database
        /// </summary>
        /// <param name="visualItem"> visualItem to be saved</param>
        /// <returns>return the newly added visualItem ID</returns>
        public int Insert(Framework.Domain.VisualItem visualItem)
        {
            try
            {
                VisualItem visualItemToInsert = Mapper.Map<VisualItem>(visualItem);
                Add(visualItemToInsert);
                Save();
                return visualItemToInsert.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// this function used to add or remove geofence from the visualItem
        /// </summary>
        /// <param name="ID">
        /// this id of the visualItem, which will be made the change
        /// </param>
        /// <param name="geofenceID"> geofence to add or remove</param>
        /// <param name="add"> bool indicate wheather the geofence is to add or remove</param>
        /// <returns></returns>
        public bool MangeGeoFenceVisualItem(int ID, int geoFenceID, bool add)
        {
            try
            {
                MSCGroupProject.Data.Model.VisualItem visualItem = Table.FirstOrDefault(p => p.Id == ID);
                if (add)
                {
                    visualItem.GeoFences.Add(Context.GeoFences.FirstOrDefault(v => v.Id == geoFenceID));
                }
                else
                {
                    visualItem.GeoFences.Remove(Context.GeoFences.FirstOrDefault(v => v.Id == geoFenceID));
                }
                Edit(visualItem);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// this function used to add or remove profile from the visualItem
        /// </summary>
        /// <param name="ID">
        /// this ID of the visualItem, which will be made the change
        /// </param>
        /// <param name="profileId"> profile to add or remove</param>
        /// <param name="add"> bool indicate wheather the profile is to add or remove</param>
        /// <returns></returns>
        public bool MangeProfileVisualItem(int ID, int profileId, bool add)
        {
            try
            {
                MSCGroupProject.Data.Model.VisualItem visualItem = Table.FirstOrDefault(p => p.Id == ID);
                if (add)
                {
                    visualItem.Profiles.Add(Context.Profiles.FirstOrDefault(v => v.Id == profileId));
                }
                else
                {
                    visualItem.Profiles.Remove(Context.Profiles.FirstOrDefault(v => v.Id == profileId));
                }

                Edit(visualItem);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// update a visualItem object in the database
        /// </summary>
        /// <param name="visualItem">visualItem with the updates</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Edit(Framework.Domain.VisualItem visualItem)
        {
            try
            {
                VisualItem visualItemToEdit = Mapper.Map<VisualItem>(visualItem);
                Edit(visualItemToEdit);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// delete a visualItem object from the database
        /// </summary>
        /// <param name="ID">id to obtain the visualItem</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Delete(int ID)
        {
            try
            {
                var visualItem = Table.FirstOrDefault(v => v.Id == ID);
                Delete(visualItem);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// get a specific visualITem based on the ID
        /// </summary>
        /// <param name="ID">id to obtain the correct visualItem</param>
        /// <returns>return visualItem</returns>
        public Framework.Domain.VisualItem FindVisualItemByID(int ID)
        {
            try
            {
                var visualItem = Table.FirstOrDefault(v => v.Id == ID);
                return Mapper.Map<MSCGroupProject.Framework.Domain.VisualItem>(visualItem);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// get all visualItems in the database
        /// </summary>
        /// <returns>return collection of visualITems</returns>
        public List<Framework.Domain.VisualItem> GetAllVisualItems()
        {
            try
            {
                return LoadVisualItems(GetAll());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// this function used to obtain any visualITems associated with the specific geofence
        /// </summary>
        /// <param name="geofenceId">to indicate which geofence</param>
        /// <returns>collection of visualItems</returns>
        public List<Framework.Domain.VisualItem> GetVisualItemForGeoFence(int geofenceId)
        {
            List<MSCGroupProject.Data.Model.VisualItem> visualItems = Table.Where(v => v.GeoFences.Any(p => p.Id == geofenceId)).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.VisualItem>>(visualItems);
        }
        /// <summary>
        /// this function used to obtain any visualITems associated with the specific profile
        /// </summary>
        /// <param name="profileId">to indicate which profile</param>
        /// <returns>collection of visualItems</returns>
        public List<Framework.Domain.VisualItem> GetVisualItemForProfile(int profileId)
        {
            List<MSCGroupProject.Data.Model.VisualItem> visualItems = Table.Where(v => v.Profiles.Any(p => p.Id == profileId)).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.VisualItem>>(visualItems);
        }
        /// <summary>
        /// this function used to obtain any visualITems not associated with the specific profile
        /// </summary>
        /// <param name="profileId">to indicate which profile</param>
        /// <returns>collection of visualItems</returns>
        public List<Framework.Domain.VisualItem> GetVisualItemsNotInProfile(int profileId)
        {
            var visualItems = GetAll().Except(Context.Profiles.FirstOrDefault(v => v.Id == profileId).VisualItems).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.VisualItem>>(visualItems);
        }
        /// <summary>
        /// this function maps the objects in the database to framework objects
        /// framework project contains repilication of database entities
        /// </summary>
        /// <param name="visualItemToMap">the single profile needs to mapped</param>
        /// <returns>collections of framework objects</returns>
        internal List<MSCGroupProject.Framework.Domain.VisualItem> LoadVisualItems(List<VisualItem> visualItemToMap)
        {
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.VisualItem>>(visualItemToMap);
        }
    }
}
