﻿using AutoMapper;
using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Repositories
{
    public class ProfileRepostiory : GenericRepository<GroupProjectModelContainer, MSCGroupProject.Data.Model.Profile>, IProfileRepostiory
    {
        /// <summary>
        /// Insert and save a profile to the database
        /// this function will map the profile to the database entity 
        /// and saves it to the database
        /// </summary>
        /// <param name="profile"> profile to be saved</param>
        /// <returns>return the newly added profile ID</returns>
        public int Insert(Framework.Domain.Profile profile)
        {
            try
            {
                MSCGroupProject.Data.Model.Profile profileToInsert = Mapper.Map<MSCGroupProject.Data.Model.Profile>(profile);
                Add(profileToInsert);
                Save();
                return profileToInsert.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        /// <summary>
        /// update a geofence object in the database
        /// </summary>
        /// <param name="geofence">geofence with the updates</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Edit(Framework.Domain.Profile profile)
        {
            try
            {
                MSCGroupProject.Data.Model.Profile profileToEdit = Mapper.Map<MSCGroupProject.Data.Model.Profile>(profile);
                Edit(profileToEdit);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// this function used to add or remove geofence from the profile
        /// </summary>
        /// <param name="ID">
        /// this id of the profile, which will be made the change
        /// </param>
        /// <param name="geofenceId"> geofence to add or remove</param>
        /// <param name="add"> bool indicate wheather the geofence is to add or remove</param>
        /// <returns></returns>
        public bool MangeProfileGeofence(int ID, int geofenceId, bool add)
        {
            try
            {
                MSCGroupProject.Data.Model.Profile profile = Table.FirstOrDefault(p => p.Id == ID);
                if (add)
                {
                    profile.GeoFences.Add(Context.GeoFences.FirstOrDefault(g => g.Id == geofenceId));
                }
                else
                {
                    profile.GeoFences.Remove(Context.GeoFences.FirstOrDefault(v => v.Id == geofenceId));
                }
                Edit(profile);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// this function used to add or remove geofence from the visualItem
        /// </summary>
        /// <param name="ID">
        /// this id of the profile, which will be made the change
        /// </param>
        /// <param name="visualItemId"> id of the visualitem to add or remove</param>
        /// <param name="add"> bool indicate wheather the visualItem is to add or remove</param>
        /// <returns></returns>
        public bool MangeProfileVisualItem(int ID, int visualItemId, bool add)
        {
            try
            {
                MSCGroupProject.Data.Model.Profile profile = Table.FirstOrDefault(p => p.Id == ID);
                if (add)
                {
                    profile.VisualItems.Add(Context.VisualItems.FirstOrDefault(v => v.Id == visualItemId));
                }
                else
                {
                    profile.VisualItems.Remove(Context.VisualItems.FirstOrDefault(v => v.Id == visualItemId));
                }
                Edit(profile);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// delete a profile object from the database
        /// </summary>
        /// <param name="id">id to obtain the profile</param>
        /// <returns>success or failuer returned as bool</returns>
        public bool Delete(int ID)
        {
            try
            {
                var profile = Table.FirstOrDefault(p => p.Id == ID);
                Delete(profile);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// get a specific profile based on the ID
        /// </summary>
        /// <param name="ID">id to obtain the correct profile</param>
        /// <returns>return profile</returns>
        public Framework.Domain.Profile FindProfileByID(int ID)
        {
            try
            {
                return LoadSingleProfile(Table.FirstOrDefault(p => p.Id == ID));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// get all profiles based in the database
        /// </summary>
        /// <returns>return collection of profiles</returns>
        public List<Framework.Domain.Profile> GetAllProfiles()
        {
            try
            {
                return LoadProfiles(GetAll());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// this function used to object any profiles associated with the specific visualItem
        /// </summary>
        /// <param name="visualItemId">to indicate which visualItem</param>
        /// <returns>collection of profiles</returns>
        public List<Framework.Domain.Profile> GetProfilesForVisualItem(int visualItemId)
        {
            List<MSCGroupProject.Data.Model.Profile> profiles = Table.Where(v => v.VisualItems.Any(p => p.Id == visualItemId)).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profiles);
        }

        public List<Framework.Domain.Profile> GetProfilesForGeofence(int geofenceId)
        {
            List<MSCGroupProject.Data.Model.Profile> profiles = Table.Where(v => v.GeoFences.Any(p => p.Id == geofenceId)).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profiles);
        }
        /// <summary>
        /// this function used to object any profiles associated with the specific geofence
        /// </summary>
        /// <param name="visualItemId">to indicate which geofence</param>
        /// <returns>collection of profiles</returns>
        public List<Framework.Domain.Profile> GetProfilesForUser(int userId)
        {
            List<MSCGroupProject.Data.Model.Profile> profiles = Table.Where(v => v.Id == userId).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profiles);
        }
        /// <summary>
        /// this function used to get any profiles assosiated with given visualItem
        /// to identify which visualitem can be added to the profile
        /// </summary>
        /// <param name="ID">its the profiled ID</param>
        /// <returns>list of profiles that is currently not assied to the give visualItem</returns>
        public List<Framework.Domain.Profile> GetProfilesNotInVisualItem(int visualItemId)
        {
            var profiles = GetAll().Except(Context.VisualItems.FirstOrDefault(v => v.Id == visualItemId).Profiles).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profiles);
        }
        /// <summary>
        /// this function used to get any profiles not assosiated with given user
        /// to identify which user can be added to the profile
        /// </summary>
        /// <param name="ID">its the user ID</param>
        /// <returns>list of profiles that is currently not assied to the give user</returns>
        public List<Framework.Domain.Profile> GetProfilesNotInUser(int userId)
        {
            var profiles = GetAll().Except(Context.Users.FirstOrDefault(u => u.Id == userId).Profiles).ToList();
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profiles);
        }
        /// <summary>
        /// this function maps the objects in the database to framework objects
        /// framework project contains repilication of database entities
        /// </summary>
        /// <param name="profileToMap">the single profile needs to mapped</param>
        /// <returns>returns the profile as object from the framework project</returns>
        internal MSCGroupProject.Framework.Domain.Profile LoadSingleProfile(MSCGroupProject.Data.Model.Profile profileToMap)
        {
            return Mapper.Map<MSCGroupProject.Framework.Domain.Profile>(profileToMap);
        }
        /// <summary>
        /// this function maps collections of profiles from database to the framework objects
        /// framwork project contains a replication of database entities
        /// </summary>
        /// <param name="profilesToMap">its the collection of database profiles</param>
        /// <returns>collections of framework objects</returns>
        internal List<MSCGroupProject.Framework.Domain.Profile> LoadProfiles(List<MSCGroupProject.Data.Model.Profile> profilesToMap)
        {
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.Profile>>(profilesToMap);
        }
    }
}
