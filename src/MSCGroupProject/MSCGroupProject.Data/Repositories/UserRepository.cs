﻿using AutoMapper;
using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;
namespace MSCGroupProject.Data.Repositories
{
    public class UserRepository : GenericRepository<GroupProjectModelContainer, User>, IUserRepositiory
    {
        /// <summary>
        /// Insert and save a user to the database
        /// this function will map the user to the database entity 
        /// and saves it to the database
        /// </summary>
        /// <param name="user"> user to be saved</param>
        /// <returns></returns>
        public bool Insert(Framework.Domain.User user)
        {
            try
            {
                User userToInsert = Mapper.Map<User>(user);
                Add(userToInsert);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// this function used to add or remove profiles from the user
        /// </summary>
        /// <param name="ID">
        /// this id of the user, which will be made the change
        /// </param>
        /// <param name="profileId"> profile to add or remove</param>
        /// <param name="add"> bool indicate wheather the profile is to add or remove</param>
        /// <returns></returns>
        
        public bool MangeUserProfiles(int ID, int profileId, bool add)
        {
            try
            {
                MSCGroupProject.Data.Model.User user = Table.FirstOrDefault(p => p.Id == ID);
                if (add)
                {
                    user.Profiles.Add(Context.Profiles.FirstOrDefault(v => v.Id == profileId));
                }
                else
                {
                    user.Profiles.Remove(Context.Profiles.FirstOrDefault(v => v.Id == profileId));
                }

                Edit(user);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update(Framework.Domain.User user)
        {
            try
            {
                User userToInsert = Mapper.Map<User>(user);
                Table.Attach(userToInsert);
                Edit(userToInsert);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool Update(User user)
        {
            try
            {
                Table.Attach(user);
                Edit(user);
                Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// get all users based in the database
        /// </summary>
        /// <returns>return collection of users</returns>
        public List<Framework.Domain.User> GetAllUsers()
        {
            try
            {
                return LoadUsers(GetAll());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// get a specific user based on the ID
        /// </summary>
        /// <param name="ID">id to obtain the correct user</param>
        /// <returns>return user</returns>
        public Framework.Domain.User FindUserByID(int ID)
        {
            try
            {
                return LoadSingleUser(Table.FirstOrDefault(u => u.Id == ID));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public Framework.Domain.User Login(string username, string password, bool isApp)
        {
            try
            {
                var dbUser = FindSingle(u => u.Username == username);
                if (dbUser != null)
                {
                    var loadedUser = LoadSingleUser(dbUser);

                    if (loadedUser.IsCorrectPassword(password))
                    {
                        if (isApp)
                        {
                        dbUser.UserToken = BCryptHelper.GenerateSalt();
                        if (Update(dbUser)) 
                        {
                            loadedUser.UserToken = dbUser.UserToken;
                        }
                    }
                    }
                    else
                    {
                        loadedUser = null;
                    }

                    return loadedUser != null ? loadedUser : null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public Framework.Domain.User Login(string userToken)
        {
            return LoadSingleUser(FindSingle(u => u.UserToken == userToken));
        }
        /// <summary>
        /// this function maps the objects in the database to framework objects
        /// framework project contains repilication of database entities
        /// </summary>
        /// <param name="userToMap">the single user needs to mapped</param>
        /// <returns>returns the user as object from the framework project</returns>
        internal MSCGroupProject.Framework.Domain.User LoadSingleUser(User userToMap)
                {
            return Mapper.Map<MSCGroupProject.Framework.Domain.User>(userToMap);
            }
        /// <summary>
        /// this function maps collections of users from database to the framework objects
        /// framwork project contains a replication of database entities
        /// </summary>
        /// <param name="usersToMap">its the collection of database users</param>
        /// <returns>collections of framework objects</returns>
        internal List<MSCGroupProject.Framework.Domain.User> LoadUsers(List<User> usersToMap)
            {
            return Mapper.Map<List<MSCGroupProject.Framework.Domain.User>>(usersToMap);
        }
    }
}
