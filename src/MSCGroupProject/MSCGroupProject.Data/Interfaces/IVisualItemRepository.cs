﻿using System;
using System.Collections.Generic;
namespace MSCGroupProject.Data.Interfaces
{
    public interface IVisualItemRepository
    {
        int Insert(MSCGroupProject.Framework.Domain.VisualItem visualItem);
        bool MangeGeoFenceVisualItem(int ID, int geoFenceID, bool add);
        bool MangeProfileVisualItem(int ID, int profileId, bool add);
        bool Edit(MSCGroupProject.Framework.Domain.VisualItem visualItem);
        bool Delete(int ID);
        MSCGroupProject.Framework.Domain.VisualItem FindVisualItemByID(int ID);
        List<MSCGroupProject.Framework.Domain.VisualItem> GetAllVisualItems();
        List<MSCGroupProject.Framework.Domain.VisualItem> GetVisualItemForGeoFence(int geofenceId);
        List<MSCGroupProject.Framework.Domain.VisualItem> GetVisualItemForProfile(int profileId);
        List<MSCGroupProject.Framework.Domain.VisualItem> GetVisualItemsNotInProfile(int profileId);
    }
}
