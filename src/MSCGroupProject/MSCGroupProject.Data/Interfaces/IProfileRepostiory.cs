﻿using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Interfaces
{
    public interface IProfileRepostiory 
    {
        int Insert(MSCGroupProject.Framework.Domain.Profile profile);
        bool Edit(MSCGroupProject.Framework.Domain.Profile profile);
        bool MangeProfileGeofence(int ID, int geofenceId, bool add);
        bool MangeProfileVisualItem(int ID, int visualItemId, bool add);
        bool Delete(int id);
        MSCGroupProject.Framework.Domain.Profile FindProfileByID(int ID);
        List<MSCGroupProject.Framework.Domain.Profile> GetAllProfiles();
        List<Framework.Domain.Profile> GetProfilesForVisualItem(int visualItemId);
        List<Framework.Domain.Profile> GetProfilesForGeofence(int geofenceId);
        List<Framework.Domain.Profile> GetProfilesForUser(int userId);
        List<Framework.Domain.Profile> GetProfilesNotInVisualItem(int visualItemId);
        List<Framework.Domain.Profile> GetProfilesNotInUser(int userId);
    }
}
