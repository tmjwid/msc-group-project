﻿using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Interfaces
{
    public interface IGeoFenceRepositiory
    {
        int Insert(MSCGroupProject.Framework.Domain.GeoFence geoFence);
        bool Edit(Framework.Domain.GeoFence geofence);
        bool Delete(int ID);
        MSCGroupProject.Framework.Domain.GeoFence FindGeoFenceByID(int ID);
        List<MSCGroupProject.Framework.Domain.GeoFence> GetAllGeoFences();
        List<Framework.Domain.GeoFence> GetGeoFencesForVisualItem(int visualItemId);
        List<Framework.Domain.GeoFence> GetGeoFencesForProfile(int profileId);
        MSCGroupProject.Framework.Domain.GeoFence FindGeoFenceByID(int ID);
        IEnumerable<Framework.Domain.GeoFence> GetGeoFencesNotInVisualItem(int ID);
        bool Edit(Framework.Domain.GeoFence geofence);
        bool Delete(int id);
        List<Framework.Domain.GeoFence> GetGeoFencesNotInProfile(int id);
    }
}
