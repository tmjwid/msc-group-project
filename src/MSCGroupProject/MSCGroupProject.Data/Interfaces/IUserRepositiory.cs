﻿using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Interfaces
{
    public interface IUserRepositiory 
    {
        bool Insert(MSCGroupProject.Framework.Domain.User user);
        bool MangeUserProfiles(int ID, int profileId, bool add);
        List<MSCGroupProject.Framework.Domain.User> GetAllUsers();
        MSCGroupProject.Framework.Domain.User FindUserByID(int ID);

        MSCGroupProject.Framework.Domain.User Login(string username, string password, bool isApp);

        MSCGroupProject.Framework.Domain.User Login(string userToken);
        bool MangeUserProfiles(int ID, int profileId, bool add);
    }
}
