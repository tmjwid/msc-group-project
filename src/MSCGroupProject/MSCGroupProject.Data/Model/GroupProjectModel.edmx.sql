
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/21/2015 22:51:15
-- Generated from EDMX file: E:\Tom\Documents\msc-group-project\src\MSCGroupProject\MSCGroupProject.Data\Model\GroupProjectModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GroupProject];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ProfileVisualItem_Profile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProfileVisualItem] DROP CONSTRAINT [FK_ProfileVisualItem_Profile];
GO
IF OBJECT_ID(N'[dbo].[FK_ProfileVisualItem_VisualItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProfileVisualItem] DROP CONSTRAINT [FK_ProfileVisualItem_VisualItem];
GO
IF OBJECT_ID(N'[dbo].[FK_VisualItemGeoFence_VisualItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VisualItemGeoFence] DROP CONSTRAINT [FK_VisualItemGeoFence_VisualItem];
GO
IF OBJECT_ID(N'[dbo].[FK_VisualItemGeoFence_GeoFence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VisualItemGeoFence] DROP CONSTRAINT [FK_VisualItemGeoFence_GeoFence];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile] DROP CONSTRAINT [FK_UserProfile_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile_Profile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserProfile] DROP CONSTRAINT [FK_UserProfile_Profile];
GO
IF OBJECT_ID(N'[dbo].[FK_GeoFenceProfile_GeoFence]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GeoFenceProfile] DROP CONSTRAINT [FK_GeoFenceProfile_GeoFence];
GO
IF OBJECT_ID(N'[dbo].[FK_GeoFenceProfile_Profile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GeoFenceProfile] DROP CONSTRAINT [FK_GeoFenceProfile_Profile];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Profiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Profiles];
GO
IF OBJECT_ID(N'[dbo].[VisualItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VisualItems];
GO
IF OBJECT_ID(N'[dbo].[GeoFences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GeoFences];
GO
IF OBJECT_ID(N'[dbo].[ProfileVisualItem]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProfileVisualItem];
GO
IF OBJECT_ID(N'[dbo].[VisualItemGeoFence]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VisualItemGeoFence];
GO
IF OBJECT_ID(N'[dbo].[UserProfile]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserProfile];
GO
IF OBJECT_ID(N'[dbo].[GeoFenceProfile]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GeoFenceProfile];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Firstname] nvarchar(max)  NOT NULL,
    [Lastname] nvarchar(max)  NOT NULL,
    [UserToken] nvarchar(max)  NULL
);
GO

-- Creating table 'Profiles'
CREATE TABLE [dbo].[Profiles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Catagory] nvarchar(max)  NOT NULL,
    [IsDefault] bit  NOT NULL
);
GO

-- Creating table 'VisualItems'
CREATE TABLE [dbo].[VisualItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Upload] varbinary(max)  NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [IsPrivate] bit  NOT NULL
);
GO

-- Creating table 'GeoFences'
CREATE TABLE [dbo].[GeoFences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Lat] nvarchar(max)  NOT NULL,
    [Long] nvarchar(max)  NOT NULL,
    [Radius] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProfileVisualItem'
CREATE TABLE [dbo].[ProfileVisualItem] (
    [Profiles_Id] int  NOT NULL,
    [VisualItems_Id] int  NOT NULL
);
GO

-- Creating table 'VisualItemGeoFence'
CREATE TABLE [dbo].[VisualItemGeoFence] (
    [VisualItems_Id] int  NOT NULL,
    [GeoFences_Id] int  NOT NULL
);
GO

-- Creating table 'UserProfile'
CREATE TABLE [dbo].[UserProfile] (
    [Users_Id] int  NOT NULL,
    [Profiles_Id] int  NOT NULL
);
GO

-- Creating table 'GeoFenceProfile'
CREATE TABLE [dbo].[GeoFenceProfile] (
    [GeoFences_Id] int  NOT NULL,
    [Profiles_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Profiles'
ALTER TABLE [dbo].[Profiles]
ADD CONSTRAINT [PK_Profiles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VisualItems'
ALTER TABLE [dbo].[VisualItems]
ADD CONSTRAINT [PK_VisualItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GeoFences'
ALTER TABLE [dbo].[GeoFences]
ADD CONSTRAINT [PK_GeoFences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Profiles_Id], [VisualItems_Id] in table 'ProfileVisualItem'
ALTER TABLE [dbo].[ProfileVisualItem]
ADD CONSTRAINT [PK_ProfileVisualItem]
    PRIMARY KEY CLUSTERED ([Profiles_Id], [VisualItems_Id] ASC);
GO

-- Creating primary key on [VisualItems_Id], [GeoFences_Id] in table 'VisualItemGeoFence'
ALTER TABLE [dbo].[VisualItemGeoFence]
ADD CONSTRAINT [PK_VisualItemGeoFence]
    PRIMARY KEY CLUSTERED ([VisualItems_Id], [GeoFences_Id] ASC);
GO

-- Creating primary key on [Users_Id], [Profiles_Id] in table 'UserProfile'
ALTER TABLE [dbo].[UserProfile]
ADD CONSTRAINT [PK_UserProfile]
    PRIMARY KEY CLUSTERED ([Users_Id], [Profiles_Id] ASC);
GO

-- Creating primary key on [GeoFences_Id], [Profiles_Id] in table 'GeoFenceProfile'
ALTER TABLE [dbo].[GeoFenceProfile]
ADD CONSTRAINT [PK_GeoFenceProfile]
    PRIMARY KEY CLUSTERED ([GeoFences_Id], [Profiles_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Profiles_Id] in table 'ProfileVisualItem'
ALTER TABLE [dbo].[ProfileVisualItem]
ADD CONSTRAINT [FK_ProfileVisualItem_Profile]
    FOREIGN KEY ([Profiles_Id])
    REFERENCES [dbo].[Profiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [VisualItems_Id] in table 'ProfileVisualItem'
ALTER TABLE [dbo].[ProfileVisualItem]
ADD CONSTRAINT [FK_ProfileVisualItem_VisualItem]
    FOREIGN KEY ([VisualItems_Id])
    REFERENCES [dbo].[VisualItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileVisualItem_VisualItem'
CREATE INDEX [IX_FK_ProfileVisualItem_VisualItem]
ON [dbo].[ProfileVisualItem]
    ([VisualItems_Id]);
GO

-- Creating foreign key on [VisualItems_Id] in table 'VisualItemGeoFence'
ALTER TABLE [dbo].[VisualItemGeoFence]
ADD CONSTRAINT [FK_VisualItemGeoFence_VisualItem]
    FOREIGN KEY ([VisualItems_Id])
    REFERENCES [dbo].[VisualItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GeoFences_Id] in table 'VisualItemGeoFence'
ALTER TABLE [dbo].[VisualItemGeoFence]
ADD CONSTRAINT [FK_VisualItemGeoFence_GeoFence]
    FOREIGN KEY ([GeoFences_Id])
    REFERENCES [dbo].[GeoFences]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VisualItemGeoFence_GeoFence'
CREATE INDEX [IX_FK_VisualItemGeoFence_GeoFence]
ON [dbo].[VisualItemGeoFence]
    ([GeoFences_Id]);
GO

-- Creating foreign key on [Users_Id] in table 'UserProfile'
ALTER TABLE [dbo].[UserProfile]
ADD CONSTRAINT [FK_UserProfile_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Profiles_Id] in table 'UserProfile'
ALTER TABLE [dbo].[UserProfile]
ADD CONSTRAINT [FK_UserProfile_Profile]
    FOREIGN KEY ([Profiles_Id])
    REFERENCES [dbo].[Profiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProfile_Profile'
CREATE INDEX [IX_FK_UserProfile_Profile]
ON [dbo].[UserProfile]
    ([Profiles_Id]);
GO

-- Creating foreign key on [GeoFences_Id] in table 'GeoFenceProfile'
ALTER TABLE [dbo].[GeoFenceProfile]
ADD CONSTRAINT [FK_GeoFenceProfile_GeoFence]
    FOREIGN KEY ([GeoFences_Id])
    REFERENCES [dbo].[GeoFences]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Profiles_Id] in table 'GeoFenceProfile'
ALTER TABLE [dbo].[GeoFenceProfile]
ADD CONSTRAINT [FK_GeoFenceProfile_Profile]
    FOREIGN KEY ([Profiles_Id])
    REFERENCES [dbo].[Profiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GeoFenceProfile_Profile'
CREATE INDEX [IX_FK_GeoFenceProfile_Profile]
ON [dbo].[GeoFenceProfile]
    ([Profiles_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------