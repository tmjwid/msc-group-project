﻿using AutoMapper;
using MSCGroupProject.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Data.Mappings
{
    public class MappingHelper
    {
        public static void CreateMappings()
        {
            Mapper.CreateMap<Framework.Domain.User, User>();
            Mapper.CreateMap<User, Framework.Domain.User>();

            Mapper.CreateMap<Framework.Domain.GeoFence, GeoFence>().ForMember(x => x.Profiles, opt => opt.Ignore()); 
            Mapper.CreateMap<GeoFence, Framework.Domain.GeoFence>();

            Mapper.CreateMap<Framework.Domain.Profile, MSCGroupProject.Data.Model.Profile>();
            Mapper.CreateMap<MSCGroupProject.Data.Model.Profile, Framework.Domain.Profile>();

            Mapper.CreateMap<Framework.Domain.VisualItem, MSCGroupProject.Data.Model.VisualItem>().ForMember(x => x.Profiles, opt => opt.Ignore());
            Mapper.CreateMap<MSCGroupProject.Data.Model.VisualItem, Framework.Domain.VisualItem>();
        }

    }
}
