﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.ViewModels
{
    /// <summary>
    /// VIewmodel class used for the UI and data binding. THis is the object used to display data on the UI
    /// </summary>
    public class GeoFence
    {
        public int Id { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Radius { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }
        public ObservableCollection<VisualItem> VisualItems { get; set; }
        public Profile Profile { get; set; }
    }
}
