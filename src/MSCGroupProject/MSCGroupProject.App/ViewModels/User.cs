﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.ViewModels
{
    /// <summary>
    /// VIewmodel class used for the UI and data binding. THis is the object used to display data on the UI
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string UserToken { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime LoggedIn { get; set; }
        public ObservableCollection<Profile> Profiles { get; set; }
    }
}
