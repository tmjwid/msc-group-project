﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.ViewModels
{
    /// <summary>
    /// Viewmodel class used for the UI and data binding. THis is the object used to display data on the UI
    /// </summary>
    public class Profile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public ObservableCollection<VisualItem> VisualItems { get; set; }
        public ObservableCollection<GeoFence> GeoFences { get; set; }
        public ObservableCollection<User> Users { get; set; }
    }
}
