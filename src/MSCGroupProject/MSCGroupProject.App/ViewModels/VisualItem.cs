﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MSCGroupProject.App.ViewModels
{
    /// <summary>
    /// VIewmodel class used for the UI and data binding. THis is the object used to display data on the UI
    /// </summary>
    public class VisualItem
    {
        private string _text;
        private string _privateText;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Text
        {
            // checks if text is private then sets the text to This text is private, and store the text for retrieval 
            get
            {
                if (IsPrivate)
                {
                    _privateText = _text;
                    return "This text is private";
                }
                return _text;
            }
            set { _text = value;}
        }
        public bool IsPrivate { get; set; }

        public string PrivateText { get { return _privateText; } set { ;} }
        //returns to location of the store images for display
        public string Location { get { return ApplicationData.Current.LocalFolder.Path + "\\" + Name; } set { ;} }
        public ObservableCollection<Profile> Profiles { get; set; }
        public ObservableCollection<GeoFence> GeoFences { get; set; }
    }
}
