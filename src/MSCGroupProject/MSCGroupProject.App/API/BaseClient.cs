﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MSCGroupProject.App.API
{
    /// <summary>
    /// This is a response from the api, it contains three objects: 
    /// Object is the retrieve object from the API, this object is based on the calling requests type eg. for a user call, 
    /// the T is used to create a response with a User instantiated object
    /// For more information on what T is, see https://msdn.microsoft.com/en-us/library/ms379564%28v=vs.80%29.aspx for more information
    /// Error messages is the HTTP Error message send back from the server
    /// HttpStatusCode is the http status code for the response, 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Response<T>
    {
        public T Object { get; set; }
        public string ErrorMessage { get; set; }
        public HttpStatusCode Status { get; set; }
    }

    /// <summary>
    /// This class is a superclass to all API requests made to the API. 
    /// It implements all the shared operations used to create and process a API request and response 
    /// </summary>
    public class BaseClient
    {
        // url to the API service: live
        private const string BaseUrl = "http://mandark.org/api/";
        // url to the API service: development 
        //private const string BaseUrl = "http://localhost:15872/api/";
        //Api key so the api will accept the request, without it the api will not facilitate the request
        private string PrivateKey = "6mhg6Bgpg1Q4weGh691QD6gu05g8Y9lb";
        // a global rest client, this class performs communication with the rest services (api)
        private RestClient _client { get; set; }
        //the request to send to the api via the rest client
        protected RestRequest Request;

        public BaseClient()
        {
            //initialising a client
            _client = new RestClient(BaseUrl);
        }
        /// <summary>
        /// Creates a request for the client to perform, this will be executed when ExecuteRequest is called
        /// </summary>
        /// <param name="controller">This is the endpoint you wish to call on the api</param>
        /// <param name="method">HttpMethof to call on the API (GET, POST)</param>
        protected void CreateRequest(string controller, HttpMethod method)
        {
            //create a new request based on where the user wants to consume 
            Request = new RestRequest(controller, method);
            //add the Apikey needed to communicate to the headers. 
            Request.AddHeader("Apikey", PrivateKey);
            //if a users is logged into the app, send up their usertoken to the server
            if (App.loggedInUser != null)
            {
                Request.AddHeader("UserToken", App.loggedInUser.UserToken);
            }
        }
        /// <summary>
        /// This method executes a requests to the server, this request is based on the one created by CreateRequest:
        /// THe ExecuteRequest requires the returned data type to be established, this is again done through generics. 
        /// This is know to developers through api documentations. This type is used to deserialise the data retrieved by the api
        /// 
        /// This method is asyncrones in nature, please use await to stop the UI from blocking
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected async Task<Response<T>> ExecuteRequest<T>()
        {
            try
            {
                //executes the request to the api asyncronsly, perform on a separate thread to the UI thread
                var response = await _client.Execute<T>(Request);

                //when a responce has been recieved, it is checked to see if there has been any error messages, 
                //if there is, these are sent back to the called for error handling
                if (response.StatusCode != System.Net.HttpStatusCode.Created && response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return new Response<T>() { ErrorMessage = response.StatusDescription, Status = response.StatusCode };
                }
                //a successful request has been fulfilled, return the data
                return new Response<T>() { Object = response.Data, Status = response.StatusCode };
            }
            catch (Exception ex)
            {
                //something went wrong, state it was a server error but provide information on the error
                return new Response<T>() { ErrorMessage = ex.Message, Status = HttpStatusCode.InternalServerError };
            }
        }
    }
}
