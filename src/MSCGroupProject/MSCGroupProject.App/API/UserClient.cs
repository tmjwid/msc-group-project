﻿using AutoMapper;
using MSCGroupProject.App.Database;
using MSCGroupProject.App.Helpers;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.PortableFramework.Domain;
using RestSharp.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.API
{
    /// <summary>
    /// Client to interact the the user API
    /// </summary>
    class UserClient : BaseClient
    {
        // api endpoint needed to interact with the user api
        private const string _controller = "user";
        /// <summary>
        /// This method calls the server to validate a user and log them into the app and service
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<Response<User>> Login(string username, string password)
        {
            //clear a logged in user if present
            App.loggedInUser = null;
            //create the request to the user api with a post request
            CreateRequest(_controller, HttpMethod.Post);
            //add to the body of the reqeust, the username and password, this is used by the api 
            Request.AddBody(new UserLogin() { Username = username, Password = password });
            //execute the request, when the request has been fulfilled the response is received
            Response<User> user = await ExecuteRequest<User>();
            //error checking to make sure the request was successful
            if (string.IsNullOrEmpty(user.ErrorMessage) && user.Object != null)
            {
                //create a new user repository, this is used to save the data to a SQLLite database
                UserRepository repo = new UserRepository();
                //map the data from the request to a DBOject, this is used to insert into the db and contains information relating to db relationships
                MSCGroupProject.Database.DBObjects.User userToSave = Mapper.Map<MSCGroupProject.Database.DBObjects.User>(user.Object);
                //loop through every profile to get all their visual items
                foreach (var profile in user.Object.Profiles)
                {
                    //loop through every visual item to get access to the image
                    foreach (var visual in profile.VisualItems)
                    {
                        //asycronsly save the image to the file system, the bytes and filename are used for the image saving methods
                        await ImageHelper.SaveImage(visual.Upload, visual.Name);
                    }
                }
                //set the singleton loggedInUser to the new logged in user
                App.loggedInUser = Mapper.Map<MSCGroupProject.App.ViewModels.User>(user.Object);
                //register all the geofences with the app so they can now be used when out and about 
                await new GeoFenceHelper().RegisterGeoFences();
            }

            return user;
        }

        /// <summary>
        /// Refresh works in the same way as login in, but requires a logged in user to work
        /// </summary>
        /// <returns></returns>
        public async Task<Response<User>> Refresh()
        {
            CreateRequest(_controller, HttpMethod.Get);
            Response<User> user = await ExecuteRequest<User>();

            if (string.IsNullOrEmpty(user.ErrorMessage) && user.Object != null)
            {
                UserRepository repo = new UserRepository();
                MSCGroupProject.Database.DBObjects.User userToSave = Mapper.Map<MSCGroupProject.Database.DBObjects.User>(user.Object);

                foreach (var profile in user.Object.Profiles)
                {
                    foreach (var visual in profile.VisualItems)
                    {
                        await ImageHelper.SaveImage(visual.Upload, visual.Name);
                    }
                }

                App.loggedInUser = Mapper.Map<MSCGroupProject.App.ViewModels.User>(user.Object);
                await new GeoFenceHelper().RegisterGeoFences();
            }

            return user;
        }
    }
}
