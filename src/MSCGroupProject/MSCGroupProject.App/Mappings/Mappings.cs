﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.Mappings
{
    /// <summary>
    /// Here are all the automapper profiles used to map one object to another with similar properties 
    /// </summary>
    public class Mappings
    {
        public static void CreateMappings()
        {
            Mapper.CreateMap<MSCGroupProject.Framework.Domain.User, MSCGroupProject.Database.DBObjects.User>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.User, MSCGroupProject.Framework.Domain.User>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.User, MSCGroupProject.Framework.Domain.User>();
            Mapper.CreateMap<MSCGroupProject.Framework.Domain.User, MSCGroupProject.App.ViewModels.User>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.User, MSCGroupProject.App.ViewModels.User>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.User, MSCGroupProject.Database.DBObjects.User>();


            Mapper.CreateMap<MSCGroupProject.Framework.Domain.Profile, MSCGroupProject.Database.DBObjects.Profile>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.Profile, MSCGroupProject.Framework.Domain.Profile>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.Profile, MSCGroupProject.Framework.Domain.Profile>();
            Mapper.CreateMap<MSCGroupProject.Framework.Domain.Profile, MSCGroupProject.App.ViewModels.Profile>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.Profile, MSCGroupProject.App.ViewModels.Profile>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.Profile, MSCGroupProject.Database.DBObjects.Profile>();


            Mapper.CreateMap<MSCGroupProject.Framework.Domain.VisualItem, MSCGroupProject.Database.DBObjects.VisualItem>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.VisualItem, MSCGroupProject.Framework.Domain.VisualItem>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.VisualItem, MSCGroupProject.Framework.Domain.VisualItem>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.VisualItem, MSCGroupProject.App.ViewModels.VisualItem>();
            Mapper.CreateMap<MSCGroupProject.Framework.Domain.VisualItem, MSCGroupProject.App.ViewModels.VisualItem>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.VisualItem, MSCGroupProject.Database.DBObjects.VisualItem>();


            Mapper.CreateMap<MSCGroupProject.Framework.Domain.GeoFence, MSCGroupProject.Database.DBObjects.GeoFence>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.GeoFence, MSCGroupProject.Framework.Domain.GeoFence>();
            Mapper.CreateMap<MSCGroupProject.Framework.Domain.GeoFence, MSCGroupProject.App.ViewModels.GeoFence>();
            Mapper.CreateMap<MSCGroupProject.Database.DBObjects.GeoFence, MSCGroupProject.App.ViewModels.GeoFence>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.GeoFence, MSCGroupProject.Framework.Domain.GeoFence>();
            Mapper.CreateMap<MSCGroupProject.App.ViewModels.GeoFence, MSCGroupProject.Database.DBObjects.GeoFence>();

        }
    }
}
