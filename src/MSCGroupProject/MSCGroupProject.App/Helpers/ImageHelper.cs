﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace MSCGroupProject.App.Helpers
{
    public class ImageHelper
    {
        /// <summary>
        /// This method is used to save images to the file system
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static async Task<string> SaveImage(byte[] imageData, string fileName) 
        {
            //find the local app folder to store the images to 
            StorageFolder folder = ApplicationData.Current.LocalFolder;
            //creata file for the image, if there already is one for the filename replace it 
            StorageFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            //open the file for IO
            using (IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                //create output stream to write to file 
                using (IOutputStream outputStream = fileStream.GetOutputStreamAt(0))
                {
                    //creat datawrite to write bytes to create data bytes for file
                    using (DataWriter dataWriter = new DataWriter(outputStream))
                    {
                        //create bytes
                        dataWriter.WriteBytes(imageData);
                        //write bytes to file
                        await dataWriter.StoreAsync();
                        //clear bites from memory
                        dataWriter.DetachStream();
                    }
                    //clear bytes form memory
                    await outputStream.FlushAsync();
                }
            }
            //return filename used
            return fileName;
        }
    }
}
