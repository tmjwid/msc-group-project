﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Devices.Geolocation.Geofencing;

namespace MSCGroupProject.App.Helpers
{
    /// <summary>
    /// Helper class for all geofence operations 
    /// </summary>
    public class GeoFenceHelper
    {
        /// <summary>
        /// Method takes all saved geofences and registers them with the os to allow for geofenceing to work 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> RegisterGeoFences()
        {
            //remove any older geofences from the system 
            GeofenceMonitor.Current.Geofences.Clear();

            //loop to enter register all geofences 
            foreach (var profile in App.loggedInUser.Profiles)
            {
                foreach (var geoFence in profile.GeoFences)
                {
                    try
                    {
                        //check to see if geofence is already registered, if is skip registering it again
                        if (!GeofenceMonitor.Current.Geofences.Any(g => g.Id == geoFence.Id.ToString()))
                        {
                            //create new basic location, this class is a wrapper for a location 
                            BasicGeoposition geoPosition = new BasicGeoposition()
                                    {
                                        Latitude = Convert.ToDouble(geoFence.Lat),
                                        Longitude = Convert.ToDouble(geoFence.Long)
                                    };
                            //creates the circle for the geofence based on the locations and radius of the circle 
                            Geocircle shape = new Geocircle(geoPosition, Convert.ToDouble(geoFence.Radius));
                            //select the modes in which we want the geofence to opperate, so it needs to trigger when entered and exited 
                            MonitoredGeofenceStates monitor = MonitoredGeofenceStates.Entered | MonitoredGeofenceStates.Exited;
                            //create a geofence with the name of geofence (this is the database id), the shape of the geofence, the notifcation modes, 
                            //denoting that we want to use it more then a single time and the time it takes for the geo to activate when entered and exited
                            Geofence geofence = new Geofence(geoFence.Id.ToString(), shape, monitor, false, TimeSpan.FromSeconds(2));
                            //save this geofence to the monitor, allowing for notications when it is entered 
                            GeofenceMonitor.Current.Geofences.Add(geofence);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
            }
            return true;
        }
    }
}
