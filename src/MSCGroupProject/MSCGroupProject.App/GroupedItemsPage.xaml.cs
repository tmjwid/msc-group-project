﻿using ApplicationSettings;
using AutoMapper;
using MSCGroupProject.App.API;
using MSCGroupProject.App.Common;
using MSCGroupProject.App.Database;
using MSCGroupProject.App.Helpers;
using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Windows.ApplicationModel.Core;
using Windows.Devices.Enumeration;
using Windows.Devices.Geolocation.Geofencing;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.SpeechSynthesis;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Grouped Items Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234231

namespace MSCGroupProject.App
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class GroupedItemsPage : Page
    {
        // provides tts for the application 
        MediaElement mediaElement;
        // sets loggedin to false
        private bool isLogin = false;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        DispatcherTimer timer = new DispatcherTimer();
        SettingsFlyout1 sf = new SettingsFlyout1();

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public GroupedItemsPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            sf.LoggedOut += sf_LoggedOut;
            sf.Refresh += sf_Refresh;
            mediaElement = this.media;
        }

        async void sf_Refresh()
        {
            // hides the settings menu
            sf.Hide();
            //sets the refreshing overlay to be shown
            refreshingoverlay.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //calls api to refresh profile
            var finsied = await new UserClient().Refresh();
            //sets the refreshing overlay to be removed
            refreshingoverlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //saves new data to offline db on device
            if (finsied.Object != null)
            {
                UserRepository repo = new UserRepository();
                MSCGroupProject.Database.DBObjects.User userToSave = Mapper.Map<MSCGroupProject.Database.DBObjects.User>(App.loggedInUser);
                await repo.Insert(userToSave);
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            //register geodence will app 
            await new GeoFenceHelper().RegisterGeoFences();
            // subscibes to events from the geofence managers, this is called when a geofence is entered or exited 
            GeofenceMonitor.Current.GeofenceStateChanged += Current_GeofenceStateChanged;
            //sets the refreshing overlay to be shown

            refreshingoverlay.Visibility = Windows.UI.Xaml.Visibility.Visible;

            //create a timer to default to default profiles if we are not in a geofence
            timer.Tick += DispatcherTimerEventHandler;
            timer.Interval = new TimeSpan(0, 0, 0, 5);
            timer.Start();

            //if user is logging in for first time, we need to save all the data they just downloaded for offline caching 
            if (isLogin)
            {
                UserRepository repo = new UserRepository();
                MSCGroupProject.Database.DBObjects.User userToSave = Mapper.Map<MSCGroupProject.Database.DBObjects.User>(App.loggedInUser);
                await repo.Insert(userToSave);
            }
        }

        private void DispatcherTimerEventHandler(object sender, object e)
        {
            //sets default profiles to the datamodel to be bound to the ui
            this.DefaultViewModel["Groups"] = App.loggedInUser.Profiles.Where(p => p.IsDefault);
            //sets header to default profiles 
            pageTitle.Text = "Default Profiles";
            //remvoes refreshing overlay 
            refreshingoverlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //stops the timer
            timer.Stop();
        }

        /// <summary>
        /// Invoked when a group header is clicked.
        /// </summary>
        /// <param name="sender">The Button used as a group header for the selected group.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>
        async void Header_Click(object sender, RoutedEventArgs e)
        {
            // Determine what group the Button instance represents
            var group = (sender as FrameworkElement).DataContext;

            // The object for controlling the speech synthesis engine (voice).
            var synth = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
            //retreave the item clicked objects, this contains the text to speak 
            MSCGroupProject.App.ViewModels.Profile item = (MSCGroupProject.App.ViewModels.Profile)group;
            // Generate the audio stream from plain text.
            //create a stream to speak the text 
            SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync(item.Name);

            // Send the stream to the media object.
            mediaElement.SetSource(stream, stream.ContentType);
            //say the text! 
            mediaElement.Play();

        }

        /// <summary>
        /// Invoked when an item within a group is clicked.
        /// </summary>
        /// <param name="sender">The GridView (or ListView when the application is snapped)
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        async void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            MSCGroupProject.App.ViewModels.VisualItem item = (MSCGroupProject.App.ViewModels.VisualItem)e.ClickedItem;

            // check if item is private
            if (item.IsPrivate)
            {
                //create new object to display text 
                var flyout = new Flyout();
                //create wrapper to hold ui elements
                var grid = new StackPanel();
                //add the header and the private text to the view
                grid.Children.Add(new TextBlock { Text = "Private text", FontSize = 30 });
                grid.Children.Add(new TextBlock { Text = item.PrivateText, FontSize = 18, TextAlignment = TextAlignment.Center });
                //set the conect to the flyout as the wrapper
                flyout.Content = grid;
                //show the wrapper to the user 
                flyout.ShowAt(sender as FrameworkElement);
            }
            else
            {
                // The object for controlling the speech synthesis engine (voice).
                var synth = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
                // Generate the audio stream from plain text.
                SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync(item.Text);

                // Send the stream to the media object.
                mediaElement.SetSource(stream, stream.ContentType);
                //say the word!
                mediaElement.Play();
            }
        }

        /// <summary>
        /// This is the subscribed event for the geofence, this is called when a geofence is entered and exited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        async void Current_GeofenceStateChanged(GeofenceMonitor sender, object args)
        {
            //get the collections of geofences entered when the event was fired 
            IReadOnlyList<GeofenceStateChangeReport> reports =
              GeofenceMonitor.Current.ReadReports();
            //loop through each entered or exited geofence 
            foreach (GeofenceStateChangeReport report in reports)
            {
                //switch to note if the geofence entered is new, this means it is a new event(so first time you've entered or exited) and not a previous one that is still stored 
                switch (report.NewState)
                {
                        // weve entered the geofence condition
                    case GeofenceState.Entered:
                        // search for the geofence that we have entered and get all the profiles for this geofence
                        var profiles = App.loggedInUser.Profiles.Where(p => p.GeoFences.Any(g => g.Id == Convert.ToInt32(report.Geofence.Id))); 
                        //set the UI databind to these new profiles and the ui will update automatically with them 
                        this.DefaultViewModel["Groups"] = profiles;
                        //run a task on the UI thread to update the header and remove the refreshing overlay if you've just entered the app for the first time
                        await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            timer.Stop();
                            pageTitle.Text = "You're at " + profiles.FirstOrDefault().GeoFences.FirstOrDefault(g => g.Id == Convert.ToInt16(report.Geofence.Id)).Name;
                            refreshingoverlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        });
                        break;
                    // weve exited the geofence condition
                    case GeofenceState.Exited:
                        if (report.NewState == GeofenceState.Exited)
                        {
                            // default back to default profiles, a new event will trigger to update if we have entered a new geofence 
                            this.DefaultViewModel["Groups"] = App.loggedInUser.Profiles.Where(p => p.IsDefault);
                            //set heaer to default profiles 
                            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                            () =>
                            {
                                pageTitle.Text = "Default Profiles";
                            });
                        }
                        break;
                    case GeofenceState.Removed:
                        if (report.RemovalReason == GeofenceRemovalReason.Used)
                        {
                            // Removed because it was single use
                        }
                        else
                        {
                            // Removed because it was expired
                        }
                        break;
                }
            }
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var parameter = e.Parameter as string;

            if (!string.IsNullOrEmpty(parameter))
            {
                isLogin = true;
            }
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void settings_Click(object sender, RoutedEventArgs e)
        {
            sf.ShowIndependent();
        }
        void sf_LoggedOut()
        {
            this.Frame.Navigate(typeof(Login));
        }
    }
}