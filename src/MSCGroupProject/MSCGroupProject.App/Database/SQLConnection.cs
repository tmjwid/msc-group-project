﻿using MSCGroupProject.Database.DBObjects;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.WinRT;
using SQLiteNetExtensionsAsync.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.App.Database
{
    /// <summary>
    /// A simple generic repository pattern implementation, allowing for simplified database operations 
    /// Using generics for the tables needed to be accessed by the subclasses 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SQLConnection<T> where T : new()
    {
        //location of database on filesystem
        string dbConnection = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "group.sqlite");
        //he connection to the db
        public SQLiteAsyncConnection connection;
        public SQLConnection()
        {
            //creates a connection to file for the db, 
            var connectionFactory = new Func<SQLiteConnectionWithLock>(() =>
                new SQLiteConnectionWithLock(new SQLitePlatformWinRT(), new SQLiteConnectionString(dbConnection, storeDateTimeAsTicks: false)));
            //creats a connection to the db
            connection = new SQLiteAsyncConnection(connectionFactory);
        }

        public async void CreateDatabase()
        {
            //creates the database file and tables
            await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFileAsync("group.sqlite", Windows.Storage.CreationCollisionOption.OpenIfExists);
            await connection.CreateTablesAsync(typeof(Profile), typeof(User), typeof(VisualItem), typeof(GeoFence), typeof(GeoFenceProfile),
                typeof(GeoFenceVisual), typeof(ProfileVisual), typeof(UserProfile)).ContinueWith((results) =>
                {
                    Debug.WriteLine(results.IsCompleted);
                }); ;
        }
        /// <summary>
        /// inserts a object to the database
        /// </summary>
        /// <param name="objectToSave"></param>
        /// <returns></returns>
        public async Task<bool> Insert(object objectToSave)
        {
            try
            {
                await connection.InsertOrReplaceWithChildrenAsync(objectToSave, true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// returns all objects in the table
        /// </summary>
        /// <returns></returns>
        public async Task<ObservableCollection<T>> ReturnAll()
        {
            try
            {
                var objects = await connection.Table<T>().ToListAsync();
                return new ObservableCollection<T>(objects);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
