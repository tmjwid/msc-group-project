﻿
using AutoMapper;
using MSCGroupProject.Database.DBObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensionsAsync.Extensions;

namespace MSCGroupProject.App.Database
{
    public class UserRepository : SQLConnection<User>
    {
        /// <summary>
        /// Returns the logged in user for the application with all their data
        /// </summary>
        /// <returns></returns>
        public async Task<MSCGroupProject.App.ViewModels.User> GetLoggedInUser()
        {
            // gets the first user in the db, there is only ever one user
            User user = await connection.Table<User>().FirstOrDefaultAsync();
            //check to make sure there is a user, if not then no one is logged in 
            if (user != null)
            {
                //goes bace to the database and returns the full logged in user with all needed data
                User fullUser = await connection.FindWithChildrenAsync<User>(user.Id, true);
                //returns that user
                return fullUser != null ? Mapper.Map<MSCGroupProject.App.ViewModels.User>(fullUser) : null;
            }
            return null;
        }
        /// <summary>
        /// deletes a logged in user, logging them out of the app
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Logout()
        {
            try
            {
                //get user
                User user = await connection.Table<User>().FirstOrDefaultAsync();
                //deleted user
                await connection.DeleteAsync(user);
                //user was deleted
                return true;
            }
            catch (Exception ex)
            {
                //user was not deleted
                return false;
            }
        }
    }
}
