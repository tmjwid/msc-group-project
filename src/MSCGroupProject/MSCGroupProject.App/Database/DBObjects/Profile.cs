﻿
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Database.DBObjects
{
    /// <summary>
    /// Bases database object used to persist data to the SQLLite db
    /// </summary>
    public class Profile
    {
        // denotes primary key for table
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(ProfileVisual), CascadeOperations = CascadeOperation.All)]
        public List<VisualItem> VisualItems { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(GeoFenceProfile), CascadeOperations = CascadeOperation.All)]
        public List<GeoFence> GeoFences { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(UserProfile), CascadeOperations = CascadeOperation.All)]        
        public List<User> Users { get; set; }
    }
}
