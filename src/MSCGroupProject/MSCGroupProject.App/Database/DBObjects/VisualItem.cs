﻿
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MSCGroupProject.Database.DBObjects
{
    /// <summary>
    /// Bases database object used to persist data to the SQLLite db
    /// </summary>
    public class VisualItem
    {
        // denotes primary key for table
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public bool IsPrivate { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(ProfileVisual), CascadeOperations = CascadeOperation.All)]
        public List<Profile> Profiles { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(GeoFenceVisual), CascadeOperations = CascadeOperation.All)]
        public List<GeoFence> GeoFences { get; set; }
    }
}
