﻿
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Database.DBObjects
{
    /// <summary>
    /// Bases database object used to persist data to the SQLLite db
    /// </summary>
    public class User
    {
        // denotes primary key for table
        [PrimaryKey]
        public int Id { get; set; }
        public string Username { get; set; }
        public string UserToken { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime LoggedIn { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(UserProfile), CascadeOperations = CascadeOperation.All)]
        public List<Profile> Profiles { get; set; }
    }
}
