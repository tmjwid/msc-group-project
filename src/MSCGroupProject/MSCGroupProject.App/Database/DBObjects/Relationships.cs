﻿using MSCGroupProject.Database.DBObjects;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Database.DBObjects
{

    /// <summary>
    /// every class in here creates the link tables needed for the many to many relationships 
    /// </summary>

    public class GeoFenceVisual
    {
        [ForeignKey(typeof(VisualItem))]
        public int VisualItemId { get; set; }
        [ForeignKey(typeof(GeoFence))]
        public int GeofenceId { get; set; }
    }

    public class GeoFenceProfile
    {
        [ForeignKey(typeof(Profile))]
        public int ProfileId { get; set; }
        [ForeignKey(typeof(GeoFence))]
        public int GeoFenceId { get; set; }
    }

    public class ProfileVisual
    {
        [ForeignKey(typeof(VisualItem))]
        public int VisualItemId { get; set; }
        [ForeignKey(typeof(Profile))]
        public int ProfileId { get; set; }
    }
    public class UserProfile
    {
        [ForeignKey(typeof(User))]
        public int UserId { get; set; }
        [ForeignKey(typeof(Profile))]
        public int ProfileId { get; set; }
    }
}
