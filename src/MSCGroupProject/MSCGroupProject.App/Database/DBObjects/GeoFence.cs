﻿
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCGroupProject.Database.DBObjects
{
    /// <summary>
    /// Bases database object used to persist data to the SQLLite db
    /// </summary>
    public class GeoFence
    {
        // denotes primary key for table
        [PrimaryKey]
        public int Id { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string Radius { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted  
        [ManyToMany(typeof(GeoFenceVisual), CascadeOperations = CascadeOperation.All)]
        public List<VisualItem> VisualItems { get; set; }
        //denotes a many to many relationship, with cascade allowing for entries to be updated and deleted   
        [ManyToMany(typeof(GeoFenceProfile), CascadeOperations = CascadeOperation.All)]
        public List<Profile> Profiles { get; set; }
    }
}
