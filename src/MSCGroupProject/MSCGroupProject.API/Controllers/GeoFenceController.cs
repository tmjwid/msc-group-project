﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Framework.Domain;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MSCGroupProject.API.Controllers
{
    public class GeoFenceController : ApiController
    {
        /// <summary>
        /// Dependancy Injecting the datalayer
        /// </summary>
        private readonly IGeoFenceRepositiory _geoFenceRepository;
        public GeoFenceController(IGeoFenceRepositiory geoFenceRepository)
        {
            _geoFenceRepository = geoFenceRepository;
        }
        /// <summary>
        /// returns all geofences, not used
        /// </summary>
        /// <returns></returns>
        public List<GeoFence> Get()
        {
            return _geoFenceRepository.GetAllGeoFences();
        }
        /// <summary>
        /// returns a geofences, not used
        /// </summary>
        /// <returns></returns>
        public GeoFence Get(int id)
        {
            return _geoFenceRepository.FindGeoFenceByID(id);
        }
        /// <summary>
        /// edits a geofences, not used
        /// </summary>
        /// <returns></returns>
        public void Post([FromBody] int id, GeoFence geofence)
        {
            geofence.Id = id;
            _geoFenceRepository.Edit(geofence);
        }
        /// <summary>
        /// inserts a geofences, not used
        /// </summary>
        /// <returns></returns>
        public void Put([FromBody]GeoFence geofence)
        {
            _geoFenceRepository.Insert(geofence);
        }

        /// <summary>
        /// deletes a geofences, not used
        /// </summary>
        /// <returns></returns>
        public void Delete(int id)
        {
            _geoFenceRepository.Delete(id);
        }
    }
}