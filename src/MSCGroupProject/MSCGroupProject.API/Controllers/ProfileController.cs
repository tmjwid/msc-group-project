﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MSCGroupProject.API.Controllers
{
    public class ProfileController : ApiController
    {
        /// <summary>
        /// Dependancy Injecting the datalayer
        /// </summary>
        private readonly IProfileRepostiory _profileRepository;
        public ProfileController(IProfileRepostiory profileRepository)
        {
            _profileRepository = profileRepository;
        }
        /// <summary>
        /// Reurtns all profiles, not used
        /// </summary>
        /// <returns></returns>
        public List<Profile> Get()
        {
            return _profileRepository.GetAllProfiles();
        }

        /// <summary>
        /// returns a profile based on id provided, not used
        /// </summary>
        /// <returns></returns>
        public Profile Get(int id)
        {
            return _profileRepository.FindProfileByID(id);
        }

        /// <summary>
        /// edits a profiles, not used
        /// </summary>
        /// <returns></returns>
        public void Post([FromBody]int id, Profile profile)
        {
            profile.Id = id;
            _profileRepository.Edit(profile);
        }

        /// <summary>
        /// inserts a profiles, not used
        /// </summary>
        /// <returns></returns>
        public void Put([FromBody]Profile profile)
        {
            _profileRepository.Insert(profile);
        }


        /// <summary>
        /// deletes a profiles, not used
        /// </summary>
        /// <returns></returns>
        public void Delete(int id)
        {
            _profileRepository.Delete(id);
        }
    }
}