﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MSCGroupProject.API.Controllers
{
    public class VisualItemController : ApiController
    {

        /// <summary>
        /// Dependancy Injecting the datalayer
        /// </summary>
        private readonly IVisualItemRepository _visualItemRepository;
        public VisualItemController(IVisualItemRepository visualItemRepository)
        {
            _visualItemRepository = visualItemRepository;
        }
        /// <summary>
        /// Reurtns all VisualItem, not used
        /// </summary>
        /// <returns></returns>
        public List<VisualItem> Get()
        {
            return _visualItemRepository.GetAllVisualItems();
        }
        /// <summary>
        /// Reurtns a VisualItem based on id provided, not used
        /// </summary>
        /// <returns></returns>
        public VisualItem Get(int id)
        {
            return _visualItemRepository.FindVisualItemByID(id);
        }
        /// <summary>
        /// edits a VisualItem, not used
        /// </summary>
        /// <returns></returns>
        public void Post([FromBody] int id, VisualItem visualItem)
        {
            visualItem.Id = id;
            _visualItemRepository.Edit(visualItem);
        }
        /// <summary>
        /// edits a VisualItem, not used
        /// </summary>
        /// <returns></returns>
        public void Put([FromBody] VisualItem visualItem)
        {
            _visualItemRepository.Insert(visualItem);
        }

        /// <summary>
        /// deletes a VisualItem, not used
        /// </summary>
        /// <returns></returns>
        public void Delete(int id)
        {
            _visualItemRepository.Delete(id);
        }
    }
}