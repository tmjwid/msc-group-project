﻿using MSCGroupProject.Data.Interfaces;
using MSCGroupProject.Framework.Domain;
using MSCGroupProject.PortableFramework.Domain;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MSCGroupProject.API.Controllers
{
    [AllowAnonymous]
    public class UserController : ApiController
    {
        /// <summary>
        /// Dependancy Injecting the datalayer
        /// </summary>
        private readonly IUserRepositiory _userRepositiory;
        public UserController(IUserRepositiory userRepositiory)
        {
            _userRepositiory = userRepositiory;
        }

        /// <summary>
        /// used to refresh a user profile based on the logged in user token
        /// </summary>
        /// <returns></returns>
        public User Get() 
        {
            IEnumerable<string> userTokens;
            //retreaves the user token provided by the application, 
            //this is used to retreave a user once they're logged in
            Request.Headers.TryGetValues("UserToken", out userTokens);
            //checks to see if a usertoken was found, and then sets the usertoken 
            string userToken = userTokens != null ? userTokens.FirstOrDefault() : string.Empty;
            //checks to see if the usertoek is null or not
            if (!string.IsNullOrEmpty(userToken))
            {
                //as userToken is relteaved from the datalayer based on the usertoken supplied, 
                //if there is one then one if returned if not null is returned
                User user = _userRepositiory.Login(userToken);
                //returns the user to the client calling via https or nothing and an error code if no user is found
                return user != null ? user : null;
            }
            return null;
        }


        //this allows this method to be called even though no user is logged in, a requirement because we couldn't log in without it 
        [AllowAnonymous]
        // POST api/<controller>
        public User Post([FromBody]UserLogin login)
        {
            //returns the user to log in based on supplied username and password, 
            //a true parameter is passed to say it is a call from api and not from web service. 
            //this is used to know to create a usertoken when logged in
            return _userRepositiory.Login(login.Username, login.Password, true);
        }
    }
}