﻿using API.HttpHelper;
using MSCGroupProject.Data.Repositories;
using MSCGroupProject.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API.ApiKeyHelper
{
    //﻿public class ApiKeyAuthorizeAttribute : ActionFilterAttribute
    // {
    //     ApiKeyRepository apiKeyRespository = new ApiKeyRepository();

    //     private const string ApiKeyHeaderName = "ApiKey";
    //     private const string DeveloperIDName = "DeveloperID";
    //     private const string UserToken = "UserToken";

    //     public override void OnActionExecuting(HttpActionContext filterContext)
    //     {
    //         // Get ApiKey header data
    //         IEnumerable<string> apiKeys;
    //         IEnumerable<string> developerIDs;
    //         IEnumerable<string> userTokens;

    //         filterContext.Request.Headers.TryGetValues(ApiKeyHeaderName, out apiKeys);
    //         filterContext.Request.Headers.TryGetValues(DeveloperIDName, out developerIDs);
    //         filterContext.Request.Headers.TryGetValues(UserToken, out userTokens);



    //         string apiKey = apiKeys != null ? apiKeys.FirstOrDefault() : string.Empty;
    //         string developerID = developerIDs != null ? developerIDs.FirstOrDefault() : string.Empty;
    //         string userToken = userTokens != null ? userTokens.FirstOrDefault() : string.Empty;


    //         // Check API key for not being assigned or that the key is not valid
    //         if (apiKey == null || !apiKeyRespository.IsKeyValid(developerID, apiKey))
    //         {
    //             filterContext.Response = HttpMessageHelper.CreateMessage(filterContext.Request, HttpStatusCode.Unauthorized, "The api key provided is not valid");
    //         }

    //         if (!string.IsNullOrEmpty(userToken))
    //         {
    //             User user = new UserRepository().LoginWithToken(userToken);
    //             GenericIdentity userIdentity = new GenericIdentity(user.Name);
    //             filterContext.RequestContext.Principal = new GenericPrincipal(userIdentity, user.Roles);
    //         }

    //         var test = filterContext.Request.Content.ReadAsStringAsync().Result;

    //         base.OnActionExecuting(filterContext);
    //     }
    // }

    public class AuthorizationHeaderHandler : DelegatingHandler
    {
        private string PrivateKey = "6mhg6Bgpg1Q4weGh691QD6gu05g8Y9lb";

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IEnumerable<string> apiKeys;
            IEnumerable<string> userTokens;

            if (request.Headers.TryGetValues("ApiKey", out apiKeys))
            {
                var apiKeyHeaderValue = apiKeys.First();

                //string apiKey = new EncryptionHelper().BFDecryption(apiKeys.FirstOrDefault(), PrivateKey, false);
                request.Headers.TryGetValues("UserToken", out userTokens);
                string userToken = userTokens != null ? userTokens.FirstOrDefault() : string.Empty;

                //// Check API key for not being assigned or that the key is not valid
                if (apiKeyHeaderValue != null && apiKeyHeaderValue != PrivateKey)
                {
                    var response = HttpMessageHelper.CreateMessage(request, HttpStatusCode.Unauthorized, "The api key provided is not valid");
                    response.ReasonPhrase = "You have no token";
                    return Task.FromResult<HttpResponseMessage>(response);
                }

                if (!string.IsNullOrEmpty(userToken))
                {
                    User user = new UserRepository().Login(userToken);
                    if (user != null)
                    {
                        GenericIdentity userIdentity = new GenericIdentity(user.Username);
                        HttpContext.Current.User = new GenericPrincipal(userIdentity, null);
                    }
                    else
                    {
                        var noUser = HttpMessageHelper.CreateMessage(request, HttpStatusCode.Unauthorized, "User token was invalid");
                        noUser.ReasonPhrase = "Please re login";
                        return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage());
                    }

                }
                return base.SendAsync(request, cancellationToken);

            }

            var noKey = HttpMessageHelper.CreateMessage(request, HttpStatusCode.Unauthorized, "Missing api or developer header");
            noKey.ReasonPhrase = "Missing ApiKey or DeveloperID header";
            return Task.FromResult<HttpResponseMessage>(new HttpResponseMessage());
        }
    }
}