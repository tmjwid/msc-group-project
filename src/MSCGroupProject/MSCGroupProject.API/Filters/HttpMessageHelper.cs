﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace API.HttpHelper
{
    public class HttpMessageHelper
    {
        public static HttpResponseMessage CreateMessage(HttpRequestMessage Request, HttpStatusCode statusCode, object payload)
        {
            return payload is string ? Request.CreateResponse(statusCode, new HttpError((string)payload)) : Request.CreateResponse(statusCode, payload);
        }
    }
}